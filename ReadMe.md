Integrate Whatfix SDK in your Android project
=============================================

This article explains how to install and update the Whatfix Android SDK in your project.

### **Prerequisites**:

-   Android Studio

-   Android version: **7.0 & later (API level/SDK version 24 & later)**

### **Integration:**

#### Step 1: Add Whatfix Maven repo URL in your build

Add 
maven `{ url 'https://jitpack.io' }`
in build.gradle root

**Note:**
For version : **com.android.tools.build:gradle:7+** Add the above code in settings.gradle


```groovy
 repositories { 
   maven {
 
       url  'https://jitpack.io'
 
   }
}
```


#### Step 2: Add the Whatfix SDK dependency 

Add bintray link in the app gradle dependency.

To add Whatfix dependency to your project, specify the following dependency configuration in the dependencies block of your **build.gradle** file.

```groovy
implementation 'org.bitbucket.whatfix:whatfix-android:$sdk-version'
```


where $sdk-version is the latest version of the SDK available which is [![](https://jitpack.io/v/org.bitbucket.whatfix/whatfix-android.svg)](https://jitpack.io/#org.bitbucket.whatfix/whatfix-android)


#### Step 3: Initialize Whatfix in your project

Initialize Whatfix in your **Application** class using the <ent_name> and <ent_id> provided to you.

**Java:**

```java
    Whatfix.initialize(this, "<ent_name>", "<ent_id>", AppType.NATIVE.ordinal, segmentationOptions, initializeOptions);
```


**Kotlin :**

```kotlin
Whatfix.initialize(application, "<ent_name>", "<ent_id>", AppType.NATIVE.ordinal, segmentationOptions, initializeOptions)

```


Replace <ent_id> & <ent_name> with your Whatfix account ID and NAME . To get your ENT ID & ENT NAME, email us at <support@whatfix.com> or get in touch with your Account Manager.

To understand more on the segmentationOptions and initializeOptions [refer](https://bitbucket.org/whatfix/whatfix-android/src/master/ReadMeOptional.md)

#### Step 4: Add the below proguard rules

```
-dontwarn sdk.whatfix.**

-keep class sdk.whatfix.* { *; }


-keepnames class * extends android.support.v4.app.Fragment

-keepnames class * extends android.app.Fragment

-keepnames class * extends androidx.fragment.app.Fragment

-keep class org.json.** { *; }
```





###
###

For Apps that have both react and native [refer](https://bitbucket.org/whatfix/whatfix-android/src/master/reactplusnative/ReadMe.md)
