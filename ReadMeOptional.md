Setting other optional steps for Whatfix Widgets
==================================================

### 1. InitializeOptions

**Java:**

```java

    WhatfixOptions whatfixOptions = new WhatfixOptions();
    
    whatfixOptions.setCdnHost("test.whatfix.com"); - Add this option to set your custom host to serve content. Default is [cdn.whatfix.com](http://cdn.whatix.com/)
    whatfixOptions.setStartEditor(false); - Set this option to true in order to start editor directly
    whatfixOptions.setDebugEnabled(true); - Set this option to true in order to enable seeing the extra logs on Logcat
    whatfixOptions.setEditorMode(EditorMode.DEFAULT); - Add this option to change EditorMode from Default(uses multitouch and shake) to either
    whatfixOptions.setEditorDisabled(false); - Set this option to true in order to disable invocation of editor
```


**Kotlin:**

```kotlin
    val whatfixOptions = WhatfixOptions()
    
    whatfixOptions.cdnHost = "test.whatfix.com"  - Add this option to set your custom host to serve content. Default is [cdn.whatfix.com](http://cdn.whatix.com/)
    whatfixOptions.isStartEditor = false  - Set this option to true in order to start editor directly
    whatfixOptions.isDebugEnabled = true  - Set this option to true in order to enable seeing the extra logs on Logcat
    whatfixOptions.editorMode = EditorMode.DEFAULT  - Add this option to change EditorMode from Default(uses multitouch and shake) to either
    whatfixOptions.isEditorDisabled = false  - Set this option to true in order to disable invocation of editor
```


### 2. SegmentationOptions

```java

    SegmentationOptions segmentationOptions = new SegmentationOptions();

    segmentationOptions.setLoggedInUserRole( "john_doe"); - Add this option to set the current user id for analytics
    segmentationOptions.setLoggedInUserRole("manager"); - Add this option to set the current user role from your app
    segmentationOptions.setLanguage("fr"); - Add this option to set the current user language preference

    HashMap<String, String> hashMap = new HashMap<>();
    hashMap.put("key", "value");
    segmentationOptions.setCustomKeyValue(hashMap); - Add this option to set custom window variables for advanced segmentation (e.g. `["user_type":"paid"]`)

    Whatfix.refreshWindowVariable(segmentationOptions); - Refresh with the above segmentationOptions
```


**Kotlin:**

```kotlin
    val segmentationOptions = SegmentationOptions()
    
    segmentationOptions.loggedInUserId = "john_doe" - Add this option to set the current user id for analytics
    segmentationOptions.loggedInUserRole = "manager" - Add this option to set the current user role from your app
    segmentationOptions.language = "fr" - Add this option to set the current user language preference
    
    val hashMap = HashMap<String, String>()
    hashMap["key"] = "value"...
    segmentationOptions.customKeyValue = hashMap - Add this option to set custom window variables for advanced segmentation (e.g. `["user_type":"paid"]`)

    Whatfix.refreshWindowVariable(segmentationOptions) - Refresh with the above segmentationOptions
```


### 3. Adding Margin to TaskList widget. Values in DP

**Java:**

```java

    Whatfix.setTLMargin(left, top, right, bottom);
```


**Kotlin:**

```kotlin
    Whatfix.setTLMargin(left, top, right, bottom)
```



### 4. Set custom feature flags

        "Depth"-Depth(On isEnabled=true sets the depth back to old way of calculating depth logic)
        "SoftInputMode"-SoftInputMode(On isEnabled=true, disable the setting of soft input mode to AdjustPan when the self help is open)
        "Overlay"-OverlayApproach(On isEnabled=true, enables the overlay approach to select the elements)

**Java:**

```java

    Whatfix.setFeatureFlag(flag, isEnabled) - Set the feature flag to true/false
```
`

**Kotlin:**
`
```kotlin
    Whatfix.setFeatureFlag(flag, isEnabled) - Set the feature flag to true/false
```


### 5. Set custom touch listener on app

**Java:**
```java
Whatfix.setTouchListener(view, new WhatfixTouchListener() {
@Override
public boolean onTouch(View v, MotionEvent event) {
        return performAction();
      }
});
```
`

**Kotlin:**
`
```kotlin
Whatfix.setTouchListener(view) { v, event ->
    performAction()
}
```

