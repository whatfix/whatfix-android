import com.facebook.react.bridge.ReactApplicationContext;
import com.facebook.react.bridge.ReactContextBaseJavaModule;
import com.facebook.react.bridge.ReactMethod;
import com.facebook.react.bridge.ReadableMap;
import com.facebook.react.bridge.ReadableNativeMap;

import java.util.HashMap;

import sdk.whatfix.Whatfix;
import sdk.whatfix.common.SegmentationOptions;

public class RNWhatfixModule extends ReactContextBaseJavaModule {

    public RNWhatfixModule(ReactApplicationContext reactContext) {
        super(reactContext);
    }

    @ReactMethod
    public void setScreenId(String screenId) {
        Whatfix.setScreenId(screenId);
    }

    @ReactMethod
    public void refreshWindowVariable(ReadableMap segmentationMap) {
        SegmentationOptions segmentationOptions = getSegmentationOptions(segmentationMap);
        Whatfix.refreshWindowVariable(segmentationOptions);
    }

    @ReactMethod
    public static void setIsDrawerOpen(boolean status) {
        Whatfix.setIsDrawerOpen(status);
    }

    @ReactMethod
    public static void startEditor() {
        Whatfix.startEditor();
    }


    @ReactMethod
    public static void setFeatureFlag(String flag, boolean isEnabled) {
        Whatfix.setFeatureFlag(flag, isEnabled);
    }

    @ReactMethod
    public static void setTLMargin(int left, int top, int right, int bottom) {
        Whatfix.setTLMargin(left, top, right, bottom);
    }

    private SegmentationOptions getSegmentationOptions(ReadableMap segmentationMap) {
        SegmentationOptions segmentationOptions = new SegmentationOptions();
        if (segmentationMap != null) {
            if (segmentationMap.hasKey("loggedInUserId")) {
                segmentationOptions.setLoggedInUserId(segmentationMap.getString("loggedInUserId"));
            }
            if (segmentationMap.hasKey("loggedInUserRole")) {
                segmentationOptions.setLoggedInUserRole(segmentationMap.getString("loggedInUserRole"));
            }
            if (segmentationMap.hasKey("language")) {
                segmentationOptions.setLanguage(segmentationMap.getString("language"));
            }
            if (segmentationMap.hasKey("customKeyValue")) {
                ReadableMap keyValueMap = segmentationMap.getMap("customKeyValue");
                HashMap keyValue = null;
                if (keyValueMap != null) {
                    keyValue = keyValueMap.toHashMap();
                }
                segmentationOptions.setCustomKeyValue(keyValue);
            }
        }
        return segmentationOptions;
    }

    @Override
    public String getName() {
        return "RNWhatfix";
    }

}
