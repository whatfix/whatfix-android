Integrate Whatfix SDK in your Android project with both react and native screens
=============================================

### **Integration:**

#### Step 1: Integrate the native android SDK
For integration  [refer](https://bitbucket.org/whatfix/whatfix-android/src/master/ReadMe.md)
Replace Whatfix.initialize(...) AppType.NATIVE.ordinal with AppType.REACT_NATIVE_MIX.ordinal


#### Step 2: Add below 2 files into the app module of your project
 1. [RNWhatfixModule](https://bitbucket.org/whatfix/whatfix-android/src/master/reactplusnative/RNWhatfixModule.java)
 2. [ReactWhatfixPackage](https://bitbucket.org/whatfix/whatfix-android/src/master/reactplusnative/ReactWhatfixPackage.java)


#### Step 3: In the MainApplication which implements ReactApplication, add the below snippet

```java
        @Override
        protected List<ReactPackage> getPackages() {
            List<ReactPackage> packages = new PackageList(this).getPackages();
            packages.add(new ReactWhatfixPackage());
            //...Other packages
            return packages;
        }
```


#### Step 4: In order to fetch the screenNames from react, add the below snippet in the App.js

```javascript
    NativeModules.RNWhatfix.setScreenId(<screenName>);
```


### Usage: React Navigation Version 5.x

[For more details on React navigation versions](https://reactnavigation.org/docs/getting-started)

##### Supporting screen navigation using react-navigation version 5 complete example

```jsx
import React, { useEffect } from 'react';
import {NavigationContainer} from '@react-navigation/native';
import {createStackNavigator} from '@react-navigation/stack';
import { NativeModules } from 'react-native';

const Stack = createStackNavigator();

function App() {
  const routeNameRef = React.useRef();
  const navigationRef = React.useRef();

  useEffect(() => {
    NativeModules.RNWhatfix.setScreenId(<initial_screenName>);
  }, []);

  return (
    <NavigationContainer
      ref={navigationRef}
      onReady={() =>
        (routeNameRef.current = navigationRef.current.getCurrentRoute().name)
      }
      onStateChange={() => {
        const previousRouteName = routeNameRef.current;
        const currentRouteName = navigationRef.current.getCurrentRoute().name;
        if (previousRouteName !== currentRouteName) {
          NativeModules.RNWhatfix.setScreenId(<current_screenName>);
        }
        routeNameRef.current = currentRouteName;
      }}>
      <Stack.Navigator>
        <Stack.Screen name="Screen1" component={Screen1} />
        <Stack.Screen name="Screen2" component={Screen2} />
      </Stack.Navigator>
    </NavigationContainer>
  );
}

export default App;
```


        
